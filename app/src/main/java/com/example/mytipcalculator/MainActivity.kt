package com.example.mytipcalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal

class MainActivity : AppCompatActivity() {

    var tipPercent = 0.0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.calc_btn)
        val bill = findViewById<EditText>(R.id.bill_amount_et)
        val tip = findViewById<RadioGroup>(R.id.tip_btns)

        val tip_ten = findViewById<RadioButton>(R.id.tip_btn_1)
        val tip_fifteen = findViewById<RadioButton>(R.id.tip_btn_2)
        val tip_twenty = findViewById<RadioButton>(R.id.tip_btn_3)
        val tip_twenty_five = findViewById<RadioButton>(R.id.tip_btn_4)
        val tip_thirdy = findViewById<RadioButton>(R.id.tip_btn_5)



        button.setOnClickListener {

            if (tip_ten.id == tip.checkedRadioButtonId) {
                tipPercent = .10;
            } else if (tip_fifteen.id == tip.checkedRadioButtonId) {
                tipPercent = .15
            } else if (tip_twenty.id == tip.checkedRadioButtonId) {
                tipPercent = .20
            } else if (tip_twenty_five.id == tip.checkedRadioButtonId) {
                tipPercent = .25
            } else if (tip_thirdy.id == tip.checkedRadioButtonId) {
                tipPercent = .30
            }

            //Toast.makeText(this, "Hello, World!", Toast.LENGTH_LONG).show();

            if (TextUtils.isEmpty(bill.text) && tipPercent.equals(0.0)) {
                Toast.makeText(this, "You need to enter the bill and tip amount", Toast.LENGTH_LONG).show()
            } else if (TextUtils.isEmpty(bill.text) && tipPercent != 0.0){
                Toast.makeText(this, "You need to enter the bill amount", Toast.LENGTH_LONG).show()
            } else if (!TextUtils.isEmpty(bill.text) && tipPercent.equals(0.0)) {
                Toast.makeText(this, "You need to enter the tip amount", Toast.LENGTH_LONG).show()
            } else {

                var bill_amount: Double =  bill.text.toString().toDouble()
                var tip_amount: Double = bill_amount * tipPercent
                var tip_rounded: String = String.format("%.2f", tip_amount)
                var total_amount: Double = bill_amount + tip_amount
                var total_rounded: String = String.format("%.2f", total_amount)

                tip_tv.setText("The tip is: $" + tip_rounded)
                total_tv.setText("The total including tip is: $"+total_rounded)
            }

        }

    }
}